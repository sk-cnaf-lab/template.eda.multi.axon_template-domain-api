package com.skcc.template.eda.sample_domain.core.domain.entity;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public enum SampleDomainStatusVO {
    SUBMIT("SUBMIT"), 
    PLACED("PLACED"), 
    SUCCESS("SUCCESS"),
    FAIL("FAIL");

    private String status;

    SampleDomainStatusVO(String status){
        this.status = status;
    }
}
