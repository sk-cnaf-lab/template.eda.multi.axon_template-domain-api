package com.skcc.template.eda.sample_domain.core.application.service;

import com.skcc.template.eda.common.sample_domain.core.object.command.CreateSampleDomainAggregateCommand;
import com.skcc.template.eda.common.sample_domain.core.object.command.FailSampleDomainAggregateCommand;
import com.skcc.template.eda.common.sample_domain.core.object.command.SuccessSampleDomainAggregateCommand;
import com.skcc.template.eda.common.sample_domain.core.object.event.CreateSampleDomainAggregateEvent;
import com.skcc.template.eda.common.sample_domain.core.object.event.FailSampleDomainAggregateEvent;
import com.skcc.template.eda.common.sample_domain.core.object.event.SuccessSampleDomainAggregateEvent;
import com.skcc.template.eda.sample_domain.core.domain.entity.SampleDomain;
import com.skcc.template.eda.sample_domain.core.domain.entity.SampleDomainStatusVO;
import com.skcc.template.eda.sample_domain.infrastructure.persistent.axon.SampleDomainAggregate;
import com.skcc.template.eda.sample_domain.core.port_infra.external_system.ExternalSampleSystemPort;
import com.skcc.template.eda.sample_domain.infrastructure.external_system.ExternalSampleSystem;

import com.skcc.template.eda.sample_domain.infrastructure.persistent.jpa.SampleDomainRepository;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.modelling.command.Repository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Service
public class SampleDomainApplicationService implements ISampleDomainApplicationService {

    @Autowired
    private SampleDomainRepository sampleDomainRepository;
    private final ModelMapper modelmapper;
    private final CommandGateway commandGateway;
    
    @Override
    public void createSampleDomainData(CreateSampleDomainAggregateEvent event){
        log.debug("[Application Service Called] createSampleDomainData");

        try{
            SampleDomain targetData = modelmapper.map(event, SampleDomain.class);
            sampleDomainRepository.save(targetData);

        }catch(Exception e){
            commandGateway.send(FailSampleDomainAggregateCommand.builder().id(event.getId()).build());
        }
        commandGateway.send(SuccessSampleDomainAggregateCommand.builder().id(event.getId()).build());
    }

    public void successSampleDomainData(SuccessSampleDomainAggregateEvent event) throws Exception{
        log.debug("[Application Service Called] successAggregate");
        Optional<SampleDomain> optionalSampleDomain = sampleDomainRepository.findById(event.getId());
        if(optionalSampleDomain.isPresent()){
            SampleDomain targetDomain = optionalSampleDomain.get();
            targetDomain.setSampleDomainStatusVO(SampleDomainStatusVO.SUCCESS);
            sampleDomainRepository.save(targetDomain);
        }
    }

    public void failSampleDomainData(FailSampleDomainAggregateEvent event) throws Exception{
        log.debug("[Application Service Called] failSampleDomainData");
        Optional<SampleDomain> optionalSampleDomain = sampleDomainRepository.findById(event.getId());
        if(optionalSampleDomain.isPresent()){
            SampleDomain targetDomain = optionalSampleDomain.get();
            targetDomain.setSampleDomainStatusVO(SampleDomainStatusVO.FAIL);
            sampleDomainRepository.save(targetDomain);
        }
    }

}
