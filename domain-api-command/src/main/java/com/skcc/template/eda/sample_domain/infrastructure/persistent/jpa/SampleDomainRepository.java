package com.skcc.template.eda.sample_domain.infrastructure.persistent.jpa;

import com.skcc.template.eda.sample_domain.core.domain.entity.SampleDomain;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SampleDomainRepository extends JpaRepository<SampleDomain, String> {
}
