package com.skcc.template.eda.sample_domain.core.domain.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "SampleDomain")
@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class SampleDomain {
    @Id
    private String id;
    private String sampleData1;
    private String sampleData2;
    private SampleDomainStatusVO sampleDomainStatusVO;
}
