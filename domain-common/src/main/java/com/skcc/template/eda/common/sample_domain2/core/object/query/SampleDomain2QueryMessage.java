package com.skcc.template.eda.common.sample_domain2.core.object.query;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@Getter
public class SampleDomain2QueryMessage {
    private String id;
    private String domainId;
}