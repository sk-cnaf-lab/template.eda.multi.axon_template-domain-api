package com.skcc.template.eda.common.sample_domain.core.object.command;

import lombok.Builder;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.util.UUID;

@AllArgsConstructor
@ToString
@Getter
public class CreateSampleDomainAggregateCommand {

  @TargetAggregateIdentifier
  private String aggregateId;
  private String id;
  private String sampleData1;
  private String sampleData2;
  int isExternalError;

  @Builder
  public CreateSampleDomainAggregateCommand(String id, String sampleData1, String sampleData2){
    this.id = id;
    this.sampleData1 = sampleData1;
    this.sampleData2 = sampleData2;
    this.aggregateId = UUID.randomUUID().toString();
  }
}