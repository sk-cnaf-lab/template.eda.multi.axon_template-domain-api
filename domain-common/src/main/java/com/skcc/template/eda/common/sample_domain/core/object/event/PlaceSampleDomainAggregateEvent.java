package com.skcc.template.eda.common.sample_domain.core.object.event;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@ToString
@Getter
@Builder
public class PlaceSampleDomainAggregateEvent {

  private String id;
  private String status;
  private String sampleData1;
  private String sampleData2;
}