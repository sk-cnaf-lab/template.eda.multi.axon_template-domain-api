package com.skcc.template.eda.common.sample_domain2.core.object.command;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

@AllArgsConstructor
@ToString
@Getter
public class CreateSampleDomain2AggregateCommand {

  @TargetAggregateIdentifier
  private String id;
  private String sampleData1;
  private String sampleData2;
  int isExternalError;
}